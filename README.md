# yaml

使用仓颉语言开发的 yaml 文件解析 sdk

# 使用

```
[dependencies]
  yaml ={git = "https://gitcode.com/service/yaml.git", branch = "main", version = "0.0.9"}
```
 cjpm update
 
```

package example

from std import fs.*
from yaml import yaml.*

@Test
func addTest() {
    let pathStr: String = "./src/example/test.yaml"

    let filePath: Path = Path(pathStr)
    if (!File.exists(filePath)) {
        println("文件不存在")
    }

    let bs: Array<Byte> = File.readFrom(filePath)
    let config = parse(bs)

    println(config)


    let name = config.getString("complex_example.name")

    println(name)
}
```

```

HashMap{
 m: ArrayList{
   HashMap{
    d: 1
    c: 2
   }
   HashMap{
    d: 3
    c: 4
   }
  }
 l: ArrayList{
   item1
   item2
   item3
  }
 string_example: Hello, YAML!
 string_example_mut: HashMap{
   integer_example: 123
  }
 float_example: 3.14
 boolean_true: true
 boolean_false: false
 dictionary_example: HashMap{
   key1: value1
   key2: value2
   key3: HashMap{
    nested_key: nested_value
   }
  }
 null_example: ~
 another_null_example: null
 date_example: 2023-04-01
 time_example: 14:30:00
 datetime_example: 2023-04-01T14:30:00Z
 complex_example: HashMap{
   name: John Doe
   age: 30
   is_active: true
   tags: user
   attributes: HashMap{
    height: 1.80
    weight: 75.0
   }
  }
}
```
