package yaml

import std.collection.*

public func toStr(val: Any, level: Int): String {
    return match (val) {
        case str: String => str

        case str: ToString => str.toString()

        case str: HashMap<String, Any> => mapToStr(str, level)

        case str: ArrayList<Any> => listToStr(str, level)

        case _ => "*"
    }
}

func mapToStr(val: HashMap<String, Any>, level: Int): String {
    var str: String = "HashMap{\n"

    for ((k, v) in val) {
        let vStr = toStr(v, level + 1)
        str = str + echoSpace(level + 1) + k + ": ${vStr}\n"
    }

    return str + echoSpace(level) + "}"
}

func listToStr(val: ArrayList<Any>, level: Int): String {
    var str: String = "ArrayList{\n"

    for (v in val) {
        let vStr = toStr(v, level + 1)
        str = str + echoSpace(level + 1) + "${vStr}\n"
    }

    return str + echoSpace(level) + "}"
}

func echoSpace(level: Int): String {
    var s = " "
    for (_ in 0..=level : 1) {
        s = s + " "
    }
    s
}
